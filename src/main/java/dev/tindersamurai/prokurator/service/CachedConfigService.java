package dev.tindersamurai.prokurator.service;

import dev.tindersamurai.prokurator.backend.commons.service.IConfigurationService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;

@Slf4j
public class CachedConfigService implements IConfigurationService {

	private final IConfigurationService service;

	public CachedConfigService(IConfigurationService service) {
		this.service = service;
	}

	@Override
	public <T> void addConfigParam(@NonNull String s, @NonNull Class<? extends T> type, T t) {
		log.debug("addConfigParam: {}, {}, {}", s, type, t);
		service.addConfigParam(s, type, t);
	}

	@Override
	public void removeConfigParam(@NonNull String s) {
		log.debug("removeConfigParam: {}", s);
		service.removeConfigParam(s);
	}

	@Override
	public <T> void set(@NonNull String s, T t) {
		log.debug("set: {}, {}", s, t);
		service.set(s, t);
	}

	@Override @Cacheable(
			value = "config_get", key = "#s"
	) public <T> T get(@NonNull String s) {
		log.debug("get: {}", s);
		return service.get(s);
	}

	@Override @Cacheable(
			value = "config_is", key = "#s"
	) public boolean isPropExists(@NonNull String s) {
		log.debug("isPropExists: {}", s);
		return service.isPropExists(s);
	}

	@Override
	public <T> void addConfigParam(@NonNull String name, @NonNull Class<? extends T> type) {
		log.debug("addConfigParam: {}, {}", name, type);
		service.addConfigParam(name, type);
	}

	@Override @Cacheable(
			value = "config_get", key = "#name"
	) public <T> T get(@NonNull String name, @NonNull T defaultValue) {
		log.debug("get: {}, {}", name, defaultValue);
		return service.get(name, defaultValue);
	}
}
